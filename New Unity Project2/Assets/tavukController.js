﻿#pragma strict
public var tavuk : Transform;
public var yumurta : Transform;
public var uret : boolean;
public var rotasyon : Quaternion;
function Start () {
	Uretim();
	rotasyon = tavuk.rotation;

}
function Uretim () {
	uret = false;
	Instantiate(yumurta,tavuk.position + tavuk.TransformDirection(0,1,-2),Quaternion.identity);
	yield WaitForSeconds(7.0);
	uret = true;
}
function Update () {
	tavuk.rotation.x = rotasyon.x;
	tavuk.rotation.z = rotasyon.z;
	tavuk.position += tavuk.TransformDirection(Vector3.forward*10.5*Time.deltaTime); 
	if(uret){
	Uretim();
	}
}

function FixedUpdate () {
	Debug.DrawRay(tavuk.position+Vector3(0,1,0),tavuk.TransformDirection(Vector3(0,0,1))*25,Color.red);
	Debug.DrawRay(tavuk.position+Vector3(0,1,0),tavuk.TransformDirection(Vector3.left)*7,Color.green);

	if(Physics.Raycast(tavuk.position+Vector3(0,1,0),tavuk.TransformDirection(Vector3(0,0,1)),25)){


		if(Physics.Raycast(tavuk.position+Vector3(0,1,0),tavuk.TransformDirection(Vector3.left),7)){

		tavuk.Rotate(Vector3(0,65,0)*Time.deltaTime);
		}
		tavuk.Rotate(Vector3(0,-65,0)*Time.deltaTime);

	}
}
