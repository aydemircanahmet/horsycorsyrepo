﻿#pragma strict
public var oyun_ici : GameObject[];
public var pause_menu : GameObject[];
public function Duraklat () {
	for(var i=0; i<oyun_ici.length; i++ ){
		oyun_ici[i].SetActive(false);
	}
	for(var m=0; m<pause_menu.length; m++ ){
		pause_menu[m].SetActive(true);
	}
}

public function DevamEttir () {
for(var i=0; i<oyun_ici.length; i++ ){
		oyun_ici[i].SetActive(true);
	}
	for(var m=0; m<pause_menu.length; m++ ){
		pause_menu[m].SetActive(false);
	}
}