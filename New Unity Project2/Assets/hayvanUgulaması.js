﻿#pragma strict
public var vucutlar : Transform[];
public var sesler : AudioSource[];
public var hizlar : double[];
public static var no : int;
public var sola_don =false ;
public var saga_don =false ;
public var dur =false ;
public var zipla =false ;
public var durma_animler : String[];
public var idle_animler : String[];
public var zipla_animler : String[];
public var rotasyon : Vector3[] ;
public var skor_adi : String;
public var puan : int;
public var tabela : UnityEngine.UI.Text;
public var tabsur : UnityEngine.UI.Text;
public var tabhiz : UnityEngine.UI.Text;
public var sure : float;
public var sure_doldu : GameObject;
function Start () {
	no = PlayerPrefs.GetInt("oyuncu_no_HC2") ;
	sure = PlayerPrefs.GetFloat("oyun_sure_HC2");
	skor_adi = vucutlar[no].gameObject.name.Substring(0,3)+"PUAN";
	for(var m=0;m<5;m++){
		var z = vucutlar[m].gameObject.name.Substring(0,3)+"PUAN";
		PlayerPrefs.SetInt(z,0);
	}
	tabela.fontSize = Screen.height*0.1;
	tabsur.fontSize = Screen.height*0.1;
	tabhiz.fontSize = Screen.height*0.07;
	//no = 4;
	//RotasyonKayit();
}

function RotasyonKayit () {
yield WaitForSeconds(2.0);
rotasyon[0] = vucutlar[no].eulerAngles;
}
function Bitir () {

		PlayerPrefs.SetInt("mevcut_skor_HC2",puan);
		sure_doldu.SetActive(true);
		Application.LoadLevel(3);
}
function Update () {
	if(sure <= 0 ) {
	Bitir();
	return;
	}
	sure -= 0.8*Time.deltaTime;
	tabsur.text = sure.ToString("0");
	puan = PlayerPrefs.GetInt(skor_adi);
	tabela.text = puan.ToString();
	if(sola_don){
	vucutlar[no].Rotate(0,-40*Time.deltaTime,0);
	}
	if(saga_don){
	vucutlar[no].Rotate(0,40*Time.deltaTime,0);
	}
	if(zipla){
	vucutlar[no].position+=vucutlar[no].TransformDirection(Vector3(0,1,1)*5*Time.deltaTime);
	}/*
	if(!dur){
	vucutlar[no].position += vucutlar[no].TransformDirection(Vector3.forward*hizlar[no]*Time.deltaTime); 
	}*/

	if(!dur){
	vucutlar[no].position += vucutlar[no].TransformDirection(Vector3.forward*(hizlar[no]+(puan/10))*Time.deltaTime); 
	tabhiz.text = ((hizlar[no]+(puan/10))*6*3/5).ToString()+"\nKM/h";
	} else{
	tabhiz.text = "0\nKM/h";
	}
vucutlar[no].Rotate(0,Input.GetAxis("Horizontal")*70*Time.deltaTime,0);



}
public function SolaDon () {
sola_don=!sola_don;
}
public function SagaDon () {
saga_don=!saga_don;
}
public function Durma () {
DurdurmaAnimasyonFonksiyonu();
dur=!dur;

}
function DurdurmaAnimasyonFonksiyonu(){
if(!dur){
vucutlar[no].GetComponent.<Animation>().CrossFade(durma_animler[no]);
yield WaitForSeconds(1.0);
vucutlar[no].GetComponent.<Animation>().CrossFade(idle_animler[no]);
}else{
vucutlar[no].GetComponent.<Animation>().Play();
}
}
public function ZiplamaFn () {
zipla = true;
ZiplaAFN();

}
function ZiplaAFN () {
vucutlar[no].GetComponent.<Animation>().CrossFade(zipla_animler[no]);
yield WaitForSeconds(1);
zipla=false;
vucutlar[no].GetComponent.<Animation>().Play();
}
public function DuzeltFN () {
vucutlar[no].eulerAngles = rotasyon[0] ;
}
public function EveDon () {
Application.LoadLevel(0);
}
public function YenidenBasla () {
Application.LoadLevel(Application.loadedLevelName);
}