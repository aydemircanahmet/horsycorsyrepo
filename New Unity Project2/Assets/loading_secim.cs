﻿using UnityEngine;
using System.Collections;

public class loading_secim : MonoBehaviour {

	IEnumerator Load()
	{
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
		Handheld.StartActivityIndicator();
		yield return new WaitForSeconds(0);
		Application.LoadLevel(1);
	}
	IEnumerator Load2()
	{
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
		Handheld.StartActivityIndicator();
		yield return new WaitForSeconds(0);
		Application.LoadLevel(2);
	}
	public void filSEC()
	{
		PlayerPrefs.SetInt ("oyuncu_no_HC2", 0);
			
		StartCoroutine(Load());
	}
	public void timSEC()
	{
		PlayerPrefs.SetInt ("oyuncu_no_HC2", 1);

		StartCoroutine(Load());
	}
	public void ayiSEC()
	{
		PlayerPrefs.SetInt ("oyuncu_no_HC2", 2);

		StartCoroutine(Load());
	}
	public void gerSEC()
	{
		PlayerPrefs.SetInt ("oyuncu_no_HC2", 3);

		StartCoroutine(Load());
	}
	public void hipSEC()
	{
		PlayerPrefs.SetInt ("oyuncu_no_HC2", 4);

		StartCoroutine(Load());
	}
	public void ucSEC()
	{
		PlayerPrefs.SetFloat ("oyun_sure_HC2", 180);

		StartCoroutine(Load2());
	}
	public void besSEC()
	{
		PlayerPrefs.SetFloat ("oyun_sure_HC2", 300);

		StartCoroutine(Load2());
	}
	public void yediSEC()
	{
		PlayerPrefs.SetFloat ("oyun_sure_HC2", 420);

		StartCoroutine(Load2());
	}
	public void geriDon()
	{

		StartCoroutine(Load0());
	}
	IEnumerator Load0()
	{
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
		Handheld.StartActivityIndicator();
		yield return new WaitForSeconds(0);
		Application.LoadLevel(0);
	}
}
